<?php
use MultisitePosts\Plugin;
defined('ABSPATH') or die();


$settings = get_option(Plugin::OPT_PLUGIN_SETTINGS);
$postTypes = get_post_types( '' , 'names');
$sites = get_option(Plugin::OPT_SITE_SETTINGS);

?>
<style type="text/css">ul ul {padding: 10px 0px 0px 30px;font-size: 80%;}</style>
<h1>Multisite Posts Plugin Options</h1>

<form method="post" id="msp-settings">
	<p>
		<label for="msp-validSource">
			<input type="checkbox" name="validSource" id="msp-validSource"<?php if ($settings['validSource']) echo ' checked="checked"'; ?> />
			Is this site a valid source for multisite posts?
		</label>
	</p>
	<p>
		<label for="msp-validTarget">
			<input type="checkbox" name="validTarget" id="msp-validTarget"<?php if ($settings['validTarget']) echo ' checked="checked"'; ?> />
			Is this site a valid target for multisite posts?
		</label>
	</p>
	<p>
		<input type="hidden" value="0" name="shouldRefreshSites" id="msp-shouldRefreshSites"/>
		<button type="button" id="msp-refreshSites">Save and Refresh Sites</button><br/>
		<small>This may take some time, depending on the number of sites/custom post types in your WPMU installation.</small>
	</p>
	<h3>Post Types</h3>
	<p>
		<label for="msp-postTypes-source">
			Which post types can become source posts from this site? (Note: Attachments are always valid, so that attached pictures can be brought across with posts).<br/>
			<select size="16" multiple="multiple" name="postTypes-source[]" id="msp-postTypes-source">
				<?php
					foreach ($postTypes as $postType) {
						if ($postType == 'attachment') {
							continue;
						}
						echo '<option' . (in_array($postType, $settings['postTypes-source']) ? ' selected="selected">' : '>') . $postType . '</option>';
					}
				?>
			</select><br/>
			<small>Use CTRL or SHIFT to select multiple types.</small>
		</label>
	</p>
	<p>
		<label for="msp-postTypes-target">
			Which post types can become target posts from other sites? (Note: Attachments are always valid, so that attached pictures can be brought across with posts).<br/>
			<select size="16" multiple="multiple" name="postTypes-target[]" id="msp-postTypes-target">
				<?php
					foreach ($postTypes as $postType) {
						if ($postType == 'attachment') {
							continue;
						}
						echo '<option' . (in_array($postType, $settings['postTypes-target']) ? ' selected="selected">' : '>') . $postType . '</option>';
					}
				?>
			</select><br/>
			<small>Use CTRL or SHIFT to select multiple types.</small>
		</label>
	</p>
	<p>
		<button type="submit" id="msp-save">Save</button>
	</p>
	<h3>Targets for this site: (Posts from this site can be copied to these locations)
	<ul>
		<?php
			foreach ($sites['validTargets'] as $site => $types) {
				$site = get_blog_details($site,true);
				$adminurl = $site->siteurl . '/wp-admin/';
				echo '<li><a href="' . $adminurl . '">' . $site->blogname . '</a><ul>';
				foreach ($types as $type) {
					echo '<li><a href="' . $adminurl . 'edit.php?post_type=' . $type . '">' . $type . '</a></li>';
				}
				echo '</ul></li>';
			}
		?>
	</ul>
	<h3>Sources for this site: (Posts from these locations can be copied to this site)
	<ul>
		<?php
			foreach ($sites['validSources'] as $site => $types) {
				$site = get_blog_details($site,true);
				$adminurl = $site->siteurl . '/wp-admin/';
				echo '<li><a href="' . $adminurl . '">' . $site->blogname . '</a><ul>';
				foreach ($types as $type) {
					echo '<li><a href="' . $adminurl . 'edit.php?post_type=' . $type . '">' . $type . '</a></li>';
				}
				echo '</ul></li>';
			}
		?>
	</ul>
</form>
<script type="text/javascript">
jQuery( document ).ready(function($) {
	function visibility() {
		$('#msp-postTypes-source').parents('p').toggle($('#msp-validSource:checked').length>0);
		$('#msp-postTypes-target').parents('p').toggle($('#msp-validTarget:checked').length>0);
	}
	$('#msp-validSource,#msp-validTarget').change(visibility);
	visibility();
	$('#msp-refreshSites').click(function(e){
		e.preventDefault();
		$('#msp-shouldRefreshSites').val("1");
		$('#msp-settings').submit();
	});
});
</script>
