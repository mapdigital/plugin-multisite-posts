<?php
/**
 * Plugin Name: Multisite Posts plugin
 * Plugin URI: https://bitbucket.org/mapdigital/plugin-multisite-posts
 * Description: A plugin to allow posts on WPMU installations to be shared across multiple sites from a single source post.
 * Version: 1.0.1
 * Author: Chris O'Kelly
 * Author URI: https://bitbucket.org/chrisokelly/
 */

namespace MultisitePosts;
use WP_Post, WP_Query, WP_Http;

defined('ABSPATH') or die();

abstract class Plugin
{
	const OPT_PLUGIN_SETTINGS = 'multisite_post_options';
	const OPT_SITE_SETTINGS = 'multisite_site_options';
	public static $BASE;
	public static $CURRENT_SITE;
	public static $SETTINGS;
	public static $POST_FIELDS;

	public static function init()
	{
		// plugin environment
		$cls = get_class();

		self::$BASE = trailingslashit(dirname(__FILE__));
		self::$CURRENT_SITE = get_current_blog_id();
		self::$SETTINGS = self::initSettings();
		self::$POST_FIELDS = apply_filters('msp_post_fields',array(
			'post_content',
			'post_name',
			'post_title',
			'post_status',
			'post_type',
			'post_author',
			'post_password',
			'post_excerpt',
			'post_date',
			'post_date_gmt',
			'page_template',
		));

		// load up the API
		require_once(self::$BASE . 'metadata.php');

		Metadata::init();

		// add configuration UI
		add_action('admin_menu', array($cls, 'setupAdminScreens'));
		add_action('load-settings_page_multisite-posts-plugin', array($cls, 'handleOptions'));
		add_filter('plugin_action_links_' . plugin_basename(__FILE__), array($cls, 'addPluginPageLinks'));
	}

	public static function initSettings()
	{
		$settings = array(
			'plugin'	=>	get_option(self::OPT_PLUGIN_SETTINGS),
			'site'		=>	get_option(self::OPT_SITE_SETTINGS),
		);
		
		/* 
		recursively flips the information in the array, so for instance:
		$a = array(
			'a'	=> array(1,2),
			'b' => array(2,3),
			'c' => array(1,4)
		);
		$recursive_flip($a);

		gives the following:
		array(
			1 => array('a','c'),
			2 => array('a','b'),
			3 => array('b'),
			4 => array('c'),
		)
		*/
		$recursive_flip = function($a) {
			$r=array();
			foreach ($a as $k => $v) {
				foreach ($v as $j) {
					if (!array_key_exists($j, $r)) {
						$r[$j] = array();
					}
					$r[$j][] = $k;
				}
			}
			return $r;
		};

		$settings['calculated'] = array(
			'bySource'	=>	$recursive_flip($settings['site']['validSources']),
			'byTarget'	=>	$recursive_flip($settings['site']['validTargets']),
		);

		return $settings;
	}

	public static function setupAdminScreens()
	{
		add_submenu_page('options-general.php',  __('Multisite Posts Plugin'), __('Multisite Posts Plugin'), 'manage_options', 'multisite-posts-plugin', array(get_class(), 'drawSettingsPage'));
	}

	public static function drawSettingsPage()
	{
		require('options/settings.php');
	}

	public static function handleOptions()
	{
		if (empty($_POST)) {
			return;
		}

		// Refresh the options from the DB for this, rather than using the cached options
		$settings = $newSettings = get_option(self::OPT_PLUGIN_SETTINGS);
		$newSettings['validSource'] = !empty($_POST['validSource']);
		$newSettings['validTarget'] = !empty($_POST['validTarget']);
		$newSettings['postTypes-target'] = $newSettings['validTarget'] ? $_POST['postTypes-target'] : array();
		$newSettings['postTypes-source'] = $newSettings['validSource'] ? $_POST['postTypes-source'] : array();
		if ($settings != $newSettings) {
			update_option(self::OPT_PLUGIN_SETTINGS, $newSettings);
			add_action('admin_notices', array(get_class(), 'handleUpdateNotice'));
		}
		if ($_POST['shouldRefreshSites'] == "1") {
			self::updateSiteSettings($newSettings);
			add_action('admin_notices', array(get_class(), 'handleRefreshNotice'));
		} 
	}

	public static function handleUpdateNotice()
	{
		echo '<div class="updated"><p>Settings saved.</p></div>';
	}

	public static function handleRefreshNotice()
	{
		echo '<div class="updated"><p>Site options refreshed.</p></div>';
	}

	public static function addPluginPageLinks($links)
	{
		array_unshift($links, '<a href="options-general.php?page=multisite-posts-plugin">Settings</a>');
		return $links;
	}

	public static function updateSiteSettings($thisSite)
	{
		// First get a list of the blog ids
		$sites = wp_get_sites();

		$validSites = array(self::$CURRENT_SITE,);
		$validTargets = array();
		$validSources = array();
		$types = array();

		if ($thisSite['validSource']) {
			$validSources[self::$CURRENT_SITE] = $thisSite['postTypes-source'];
		}

		if ($thisSite['validTarget']) {
			$validTargets[self::$CURRENT_SITE] = $thisSite['postTypes-target'];
		}

		// Loop through each site...
		foreach ($sites as $site) {
			if ($site['blog_id'] == self::$CURRENT_SITE) {
				continue;	// ...skipping the current one...
			}

			switch_to_blog($site['blog_id']);

			$temp = get_option(self::OPT_PLUGIN_SETTINGS);
			// ... or any without a settings array
			if (!$temp) {
				restore_current_blog();
				continue;
			}

			// And grab their plugin settings
			$validSites[] = $site['blog_id'];
			if ($temp['validSource']) {
				$validSources[$site['blog_id']] = $temp['postTypes-source'];
			}
			if ($temp['validTarget']) {
				$validTargets[$site['blog_id']] = $temp['postTypes-target'];
			}

			restore_current_blog();
		}


		// Now loop through the settings to build useable options arrays and push them to each site
		foreach ($validSites as $site) {
			$temp = array(
				'validSources'	=> array(),
				'validTargets'	=> array(),
			);
			if (isset($validSources[$site])) {
				foreach ($validTargets as $targetID => $target) {
					if ($targetID == $site) {
						continue;
					}
					$temp['validTargets'][$targetID] = array_intersect($target, $validSources[$site]);
				}
			}
			if (isset($validTargets[$site])) {
				foreach ($validSources as $sourceID => $source) {
					if ($sourceID == $site) {
						continue;
					}
					$temp['validSources'][$sourceID] = array_intersect($source, $validTargets[$site]);
				}
			}
			if ($site == self::$CURRENT_SITE) {
				update_option(self::OPT_SITE_SETTINGS,$temp);
			} else {
				switch_to_blog($site);
				update_option(self::OPT_SITE_SETTINGS,$temp);
				restore_current_blog();
			}
		}
	}

	public static function error($message)
	{
		// TODO: use debug_backtrace() to get the calling function
		// TODO: put all this info in a site_option instead of straight to PHP error logs. Use a serialized array for this
		// TODO: pull out that site_option on init of the object, cache it and add to it directly through wrapper methods.
		// TODO: Add an option for the length of logs (entryLimit). 
		// TODO: Hook the latest wordpress action call; when the hook is triggered, grab the latest {entryLimit} array entries and write them back to site_options.
		// TODO: Add a tab to the settings page to view these logs.
		// TODO: Hook an admin_notice if there are new entries
		if (function_exists('error_log')) {
			error_log($message);
		}
	}

	public static function updateReplica($post, $site_id, $target_id)
	{

		// Get the post data
		$data = self::extractPostObject($post, 'update');
		if ($data === false) {
			return;
		}

		$data['ID'] = $target_id;

		// Get the meta data
		$meta = self::extractPostMeta($post, 'update');
		if ($meta === false) {
			return;
		}

		// Get the taxonomies, if applicable
		$taxonomies = self::extractPostTerms($post, 'update');
		if ($taxonomies === false) {
			return;
		}

		// Get the attachments, if applicable
		$attachments = self::extractPostAttachments($post, 'update');
		if ($attachments === false) {
			return;
		}

		$source_site = get_current_blog_id();
		$success = true;

		// Prepare to switch sites - unhook our own handler, and provide an action tag for other handlers to unhook themselves
		Metadata::bind(true);
		do_action('msp_replicating_pre_switch',$post,$site_id);

		// Switch to the target site (from this point on, no early return out of function - must deterministically reach the restore_current_blog() call)
		switch_to_blog($site_id);

			do_action('msp_replicating_post_switch',$post,$site_id);
			
			// Update the post on the target site.
			if ($success) {
				if (wp_update_post($data) === 0) {
					self::error("Attempt to update replicated post failed! Target Site: {$site_id}; Source Post ID: {$post->ID};");
				}
			}

			// Apply the attachments.
			if ($success && sizeof($attachments) > 0) {
				if(!class_exists('WP_Http')){
					include_once( ABSPATH . WPINC. '/class-http.php' );
				}

				$http = new WP_Http();
				$args = array(
					'post_type' => 'attachment',
					'posts_per_page' => -1,
					'post_status' => 'any',
					'post_parent' => $target_id,
					'fields' => 'ids',
				);

				$ids = get_posts($args);

				$args['meta_key'] = Metadata::$KEYS['source'];

				foreach ($attachments as $attachment) {
					if (!$attachment['guid']) {
						$success = false;
						self::error("Attempt to replicate post attachment failed (missing guid)! Target Site: {$site_id}; Source Post ID: {$post->ID}; Source Attachment ID: {$attachment['original_id']};");
						break;
					}

					$args['meta_value'] = $attachment['original_id'];

					$file = $http->request($attachment['guid']);
					if (is_wp_error($file)) {
						self::error("Attempt to replicate post attachment file failed! Target Site: {$site_id}; Source Post ID: {$post->ID}; Source Attachment ID: {$attachment['original_id']}; Source Attachment GUID: {$attachment['guid']}; WP Error String: {$file->get_error_msg()};");
						$success = false;
						break;
					}

					// Get the replicated attachment
					$repAttachment = get_posts($args);

					// If it exists:
					if (is_array($repAttachment) && sizeof($repAttachment) > 0) {
						$repAttachment = reset($repAttachment);
						// Remove the id from the ids array
						$ids = array_diff($ids, array($repAttachment));

						// Check if the file matches
						$repFile = $http->request(wp_get_attachment_url($repAttachment));
						if (array_key_exists('body', $repFile) && array_key_exists('body', $file) && $repFile['body'] == $file['body']) {
							// If it does, continue to next attachment
							$success = apply_filters('msp_inserted_attachment', $success, $repAttachment, $target_id, $attachment, 'update');
							continue;
						} else {
							// else, delete the replicated attachment
							wp_delete_attachment($repAttachment, true);
							$success = apply_filters('msp_deleted_attachment', $success, $repAttachment, $target_id, 'update');						
						}
					}

					// Create the attachment
					$newAttachment = wp_upload_bits( basename($attachment['guid']), null, $file['body']);
					if (array_key_exists('error', $newAttachment) && $newAttachment['error']) {
						self::error("Attempt to upload post attachment file failed! Target Site: {$site_id}; Source Post ID: {$post->ID}; Source Attachment ID: {$attachment['original_id']}; Source Attachment GUID: {$attachment['guid']}; WP Error String: {$newAttachment['error']};");
						$success = false;
						break;
					}
					
					$filetype = wp_check_filetype(basename($newAttachment['file']), null);

					$attach_id = wp_insert_attachment(array(
						'post_mime_type'=> $filetype['type'],
						'post_title'	=> $attachment['post_title'],
						'post_content'	=> '',
						'post_status'	=> 'inherit',
					), $newAttachment['file'], $target_id );

					if ($attach_id === 0) {
						self::error("Attempt to insert new attachment failed! Target Site: {$site_id}; Source Post ID: {$post->ID}; Source Attachment ID: {$attachment['original_id']}; File: {$newAttachment['file']};");
						$success = false;
						break;
					}

					if(!function_exists('wp_generate_attachment_data')) {
						require_once(ABSPATH . "wp-admin" . '/includes/image.php');
					}

					$attach_data = wp_generate_attachment_metadata($attach_id, $newAttachment['file']);
					wp_update_attachment_metadata($attach_id, $attach_data);
					// TODO: find a good way to ensure this last step is successful

					// Link back to the source attachment
					update_post_meta($attach_id, Metadata::$KEYS['source'], $attachment['original_id']);
					update_post_meta($attach_id, Metadata::$KEYS['sourceSite'], $source_site);

					// Allow user code to do other things with the newly inserted attachment (add other metadata, add the attachment to a gallery, etc). Trust them to return an honest value for success
					$success = apply_filters('msp_inserted_attachment', $success, $attach_id, $target_id, $attachment, 'update');
					if (!$success) {
						self::error("Failure during external actions! Filter: msp_inserted_attachment; Target Site: {$site_id}; Source Post ID: {$post->ID}; Source Attachment ID: {$attachment['original_id']}; New Attachment ID: {$attach_id};");
						self::error("INCONSISTENCY WARNING! Unfinished replication on post #{$target_id} on blog #{$site_id}. Restart this replication or delete the post to restore consistency.");
					}
				}
				
				foreach ($ids as $id) {
					wp_delete_attachment($id, true);
					$success = apply_filters('msp_deleted_attachment', $success, $id, $target_id, 'update');
				 }

				if (!$success) {
					self::error("Attempt to replicate post attachments failed! Target Site: {$site_id}; Source Post ID: {$post->ID};");
				}
			}

			// Apply the metadata
			if ($success) {
				self::handleOverrides($meta, $target_id);
				foreach ($meta as $key => $value) {
					$success = self::update_post_meta($target_id,$key,$value);
					if (!$success) {
						self::error("Attempt to replicate post metadata failed! Target Site: {$site_id}; Source Post ID: {$post->ID}; Target Post ID: {$target_id}; Failed Meta Key: {$key};");
						self::error("INCONSISTENCY WARNING! Unfinished replication on post #{$target_id} on blog #{$site_id}. Restart this replication or delete the post to restore consistency.");
						break;
					}
				}
			}

			// Apply the taxonomies
			if ($success) {
				self::handleAdditionalTerms($taxonomies, $target_id);
				foreach ($taxonomies as $tax_slug => $terms) {
					$success = wp_set_object_terms($target_id, $terms, $tax_slug);
					if (is_array($success)) {
						continue; // Past this point the loop will break. This breaks normal conventions for foreach, but reduces code dupe and means the expected, common path has less ops.
					}
					if (is_wp_error($success)) {
						self::error("Attempt to set post terms failed, most likely due to an invalid taxonomy slug! Target Site: {$site_id}; Source Post ID: {$post->ID}; Target Post ID: {$target_id}; Failed Taxonomy Slug: {$tax_slug}; WP Error String: {$success->get_error_msg()};");
					} elseif (is_string($success)) {
						self::error("Attempt to set post terms failed, most likely due to an invalid term! Target Site: {$site_id}; Source Post ID: {$post->ID}; Target Post ID: {$target_id}; Failed Taxonomy Slug: {$tax_slug}; Failed Term: {$success};");
					}
					self::error("INCONSISTENCY WARNING! Unfinished replication on post #{$target_id} on blog #{$site_id}. Restart this replication or delete the post to restore consistency.");
					$success = false;
					break;
				}
			}


			do_action('msp_replicating_pre_restore',$post,$site_id);

		// Restore current blog
		restore_current_blog();

		// Rebind save hooks and provide tag for other handlers to do the same
		do_action('msp_replicating_post_restore',$post,$site_id);
		Metadata::bind();
		
		// Return false an failure, or the replicated post ID on success.
		return $success ? $target_id : false;
	}

	public static function createReplica($post, $site_id)
	{
		// Get the post data
		$data = self::extractPostObject($post, 'create');
		if ($data === false) {
			return;
		}

		// Get the meta data
		$meta = self::extractPostMeta($post, 'create');
		if ($meta === false) {
			return;
		}

		// Get the taxonomies, if applicable
		$taxonomies = self::extractPostTerms($post, 'create');
		if ($taxonomies === false) {
			return;
		}

		// Get the attachments, if applicable
		$attachments = self::extractPostAttachments($post, 'create');
		if ($attachments === false) {
			return;
		}

		$source_site = get_current_blog_id();

		$success = true;

		// Prepare to switch sites - unhook our own handler, and provide an action tag for other handlers to unhook themselves
		Metadata::bind(true);
		do_action('msp_replicating_pre_switch',$post,$site_id);

		// Switch to the target site (from this point on, no early return out of function - must deterministically reach the restore_current_blog() call)
		switch_to_blog($site_id);

			do_action('msp_replicating_post_switch',$post,$site_id);
			
			// Create the post on the target site. Get the ID
			if ($success) {
				$repID = wp_insert_post($data);

				if (!$success = ($repID && $repID > 0)) {
					self::error("Attempt to replicate post failed! Target Site: {$site_id}; Source Post ID: {$post->ID};");
				} else {
					update_post_meta($repID, Metadata::$KEYS['source'], $post->ID);
					update_post_meta($repID, Metadata::$KEYS['sourceSite'], $source_site);
				}
			}

			// Apply the attachments.
			if ($success && sizeof($attachments) > 0) {
				if(!class_exists('WP_Http')){
					include_once( ABSPATH . WPINC. '/class-http.php' );
				}

				$http = new WP_Http();

				foreach ($attachments as $attachment) {
					if (!$attachment['guid']) {
						$success = false;
						self::error("Attempt to replicate post attachment failed (missing guid)! Target Site: {$site_id}; Source Post ID: {$post->ID}; Source Attachment ID: {$attachment['original_id']};");
						break;
					}
					$file = $http->request($attachment['guid']);
					if (is_wp_error($file)) {
						self::error("Attempt to replicate post attachment file failed! Target Site: {$site_id}; Source Post ID: {$post->ID}; Source Attachment ID: {$attachment['original_id']}; Source Attachment GUID: {$attachment['guid']}; WP Error String: {$success->get_error_msg()};");
						$success = false;
						break;
					}

					$newAttachment = wp_upload_bits( basename($attachment['guid']), null, $file['body']);
					if (array_key_exists('error', $newAttachment) && $newAttachment['error']) {
						self::error("Attempt to upload post attachment file failed! Target Site: {$site_id}; Source Post ID: {$post->ID}; Source Attachment ID: {$attachment['original_id']}; Source Attachment GUID: {$attachment['guid']}; WP Error String: {$newAttachment['error']};");
						$success = false;
						break;
					}
					
					$filetype = wp_check_filetype(basename($newAttachment['file']), null);

					$attach_id = wp_insert_attachment(array(
						'post_mime_type'=> $filetype['type'],
						'post_title'	=> $attachment['post_title'],
						'post_content'	=> '',
						'post_status'	=> 'inherit',
					), $newAttachment['file'], $repID );

					if ($attach_id === 0) {
						self::error("Attempt to insert new attachment failed! Target Site: {$site_id}; Source Post ID: {$post->ID}; Source Attachment ID: {$attachment['original_id']}; File: {$newAttachment['file']};");
						$success = false;
						break;
					}

					if(!function_exists('wp_generate_attachment_data')) {
						require_once(ABSPATH . "wp-admin" . '/includes/image.php');
					}

					$attach_data = wp_generate_attachment_metadata($attach_id, $newAttachment['file']);
					wp_update_attachment_metadata($attach_id, $attach_data);
					// TODO: find a good way to ensure this last step is successful

					// Link back to the source attachment
					update_post_meta($attach_id, Metadata::$KEYS['source'], $attachment['original_id']);
					update_post_meta($attach_id, Metadata::$KEYS['sourceSite'], $source_site);

					// Allow user code to do other things with the newly inserted attachment (add other metadata, add the attachment to a gallery, etc). Trust them to return an honest value for success
					$success = apply_filters('msp_inserted_attachment', $success, $attach_id, $repID, $attachment, 'create');
					if (!$success) {
						self::error("Failure during external actions! Filter: msp_inserted_attachment; Target Site: {$site_id}; Source Post ID: {$post->ID}; Source Attachment ID: {$attachment['original_id']}; New Attachment ID: {$attach_id};");
						self::error("INCONSISTENCY WARNING! Unfinished replication on post #{$repID} on blog #{$site_id}. Restart this replication or delete the post to restore consistency.");
					}
				}

				if (!$success) {
					self::error("Attempt to replicate post attachments failed! Target Site: {$site_id}; Source Post ID: {$post->ID};");
				}
			}

			// Apply the metadata
			if ($success) {
				foreach ($meta as $key => $value) {
					$success = self::update_post_meta($repID,$key,$value);
					if (!$success) {
						self::error("Attempt to replicate post metadata failed! Target Site: {$site_id}; Source Post ID: {$post->ID}; Target Post ID: {$repID}; Failed Meta Key: {$key};");
						self::error("INCONSISTENCY WARNING! Unfinished replication on post #{$repID} on blog #{$site_id}. Restart this replication or delete the post to restore consistency.");
						break;
					}
				}
			}

			// Apply the taxonomies
			if ($success) {
				foreach ($taxonomies as $tax_slug => $terms) {
					$success = wp_set_object_terms($repID, $terms, $tax_slug);
					if (is_array($success)) {
						continue; // Past this point the loop will break. This breaks normal conventions for foreach, but reduces code dupe and means the expected, common path has less ops.
					}
					if (is_wp_error($success)) {
						self::error("Attempt to set post terms failed, most likely due to an invalid taxonomy slug! Target Site: {$site_id}; Source Post ID: {$post->ID}; Target Post ID: {$repID}; Failed Taxonomy Slug: {$tax_slug}; WP Error String: {$success->get_error_msg()};");
					} elseif (is_string($success)) {
						self::error("Attempt to set post terms failed, most likely due to an invalid term! Target Site: {$site_id}; Source Post ID: {$post->ID}; Target Post ID: {$repID}; Failed Taxonomy Slug: {$tax_slug}; Failed Term: {$success};");
					}
					self::error("INCONSISTENCY WARNING! Unfinished replication on post #{$repID} on blog #{$site_id}. Restart this replication or delete the post to restore consistency.");
					$success = false;
					break;
				}
			}


			do_action('msp_replicating_pre_restore',$post,$site_id);

		// Restore current blog
		restore_current_blog();

		// Rebind save hooks and provide tag for other handlers to do the same
		do_action('msp_replicating_post_restore',$post,$site_id);
		Metadata::bind();
		
		// Return false an failure, or the replicated post ID on success.
		return $success ? $repID : false;
	}

	public static function deleteReplica($post, $site_id, $target_id)
	{
		// Prepare to switch sites - unhook our own handler, and provide an action tag for other handlers to unhook themselves
		Metadata::bind(true);
		do_action('msp_replicating_pre_switch',$post,$site_id);

		// Switch to the target site (from this point on, no early return out of function - must deterministically reach the restore_current_blog() call)
		switch_to_blog($site_id);

			do_action('msp_replicating_post_switch',$post,$site_id);
			
			// delete the attachments on the target site.
			if (apply_filters('msp_should_delete_attachments',true, $post, $site_id)) {
				self::deletePostAttachments($target_id);
			}

			// delete the post on the target site. Get the ID
			if (wp_delete_post($target_id) === false) {
				self::error("Attempt to replicate post deletion failed! Target Site: {$site_id}; Source Post ID: {$target_id};");
			}

			do_action('msp_replicating_pre_restore',$post,$site_id);

		// Restore current blog
		restore_current_blog();

		// Rebind save hooks and provide tag for other handlers to do the same
		do_action('msp_replicating_post_restore',$post,$site_id);
		Metadata::bind();
		
		// Return false an failure, or the replicated post ID on success.
		return $success;
	}

	public static function extractPostObject($post, $action)
	{
		if (!$post instanceof WP_Post) {
			self::error("Parameter passed for \$post was not a valid WP_Post object. This is an unrecoverable error, and may leave posts in an inconsistent state!");
			return false;
		}
		$extracted = array();
		foreach (self::$POST_FIELDS as $field) {
			if ($post->{$field}) {
				$extracted[$field] = apply_filters("msp_extract_post_field",$post->{$field}, $post, $action, $field);
			}
		}
		return apply_filters('msp_extract_post_fields', $extracted, $post, $action);
	}

	public static function extractPostMeta($post, $action)
	{
		if (!$post instanceof WP_Post) {
			self::error("Parameter passed for \$post was not a valid WP_Post object. This is an unrecoverable error, and may leave posts in an inconsistent state!");
			return false;
		}

		$extracted = get_post_meta($post->ID);
		if (!$extracted) {
			$extracted = array();
		}

		// remove any of our own meta keys - they should be dealt with seperately. Also remove metafields from the $DISCARDKEYS property.
		$remove = array_merge(array_flip(Metadata::$KEYS), array_flip(Metadata::$DISCARDKEYS));
		$extracted = array_diff_key($extracted, $remove);

		// Deal with singlifying the ones that need it
		foreach ($extracted as $key => $value) {
			if (self::metaSingularity($key) && is_array($value)) {
				$extracted[$key] = reset($value);
			}
		}

		return apply_filters('msp_extract_post_meta', $extracted, $post, $action);
	}

	public static function extractPostTerms($post, $action)
	{
		if (!$post instanceof WP_Post) {
			self::error("Parameter passed for \$post was not a valid WP_Post object. This is an unrecoverable error, and may leave posts in an inconsistent state!");
			return false;
		}
		$extracted = array_flip(get_object_taxonomies($post));

		// for each taxonomy, get the list of terms (both Name and Slug) in that taxonomy for this post
		foreach ($extracted as $tax_slug => &$terms) {
			$terms = wp_get_object_terms($post->ID,$tax_slug, array('fields'=>'names'));
			// Remove the key if there are no terms for it.
			if (!$terms) {
				unset($extracted[$tax_slug]);
			}
		}

		return apply_filters('msp_extract_post_terms', $extracted, $post, $action);
	}

	public static function extractPostAttachments($post, $action)
	{
		if (!$post instanceof WP_Post) {
			self::error("Parameter passed for \$post was not a valid WP_Post object. This is an unrecoverable error, and may leave posts in an inconsistent state!");
			return false;
		}
		$extracted = (array) get_posts(array(
			'post_type'		=> 'attachment',
			'numberposts'	=> -1,
			'post_parent'	=> $post->ID,
			'post_status'	=> null,
		));

		foreach ($extracted as &$attachment) {
			$temp = self::extractPostObject($attachment,'{$action}_attachments');
			$temp['meta'] = self::extractPostMeta($attachment,'{$action}_attachments');
			if (!$temp || $temp['meta'] === false) {
				return false; // Break if an attachment cannot load meta
			}
			// ensure the guid and ID gets brought over regardless of not being in POST_FIELDS
			$temp['guid'] = $attachment->guid;
			$temp['original_id'] = $attachment->ID;
			$attachment = $temp;
		}

		return apply_filters('msp_extract_post_attachments', $extracted, $post, $action);
	}

	public static function deletePostAttachments($postid)
	{
	
		global $wpdb;

		$args = array(
			'post_type'         => 'attachment',
			'post_status'       => 'any',
			'posts_per_page'    => -1,
			'post_parent'       => $postid
		);
		$attachments = new WP_Query($args);
		$attachment_ids = array();
		if($attachments->have_posts()) {
			foreach ($attachments->get_posts() as $attachment) {
				if (apply_filters('msp_should_delete_attachment', true, $attachment, $postid, $action)) {
					$attachment_ids[] = $attachment->ID;
				}
			}
		}

		if(!empty($attachment_ids)) {
			$delete_attachments_query = $wpdb->prepare('DELETE FROM %1$s WHERE %1$s.ID IN (%2$s)', $wpdb->posts, join(',', $attachment_ids));
			$wpdb->query($delete_attachments_query);
			foreach ($attachment_ids as $id) {
				unlink(get_attached_file($id));
			}
		}

	}

	public static function handleOverrides(&$meta, $id)
	{
		global $wpdb;

		$q = $wpdb->prepare("SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id = %d AND meta_key LIKE %s ", (int) $id, $wpdb->esc_like(Metadata::$KEYS['overrideMetas']) . '%');
		foreach ($wpdb->get_results($q) as $row) {
			$key = substr($row->meta_key ,strlen(Metadata::$KEYS['overrideMetas']));
			$meta[$key] = get_post_meta($id, $row->meta_key, true);
		}
	}

	public static function handleAdditionalTerms(&$taxonomies, $id)
	{
		global $wpdb;

		$q = $wpdb->prepare("SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id = %d AND meta_key LIKE %s ", (int) $id, $wpdb->esc_like(Metadata::$KEYS['addTerms']) . '%');

		foreach ($wpdb->get_results($q) as $row) {
			$key = substr($row->meta_key ,strlen(Metadata::$KEYS['addTerms']));
			$value = get_post_meta($id, $row->meta_key, true);

			if (array_key_exists($key, $taxonomies)) {
				foreach ($value as $term) {
					if (is_numeric($term) && !in_array($term, $taxonomies[$key])) {
						$taxonomies[$key][] = (int) $term;
					}
				}
			} else {
				$taxonomies[$key] = array_map('intval', $value);
			}
		}
	}

	private static function update_post_meta($id,$key,$val)
	{
		// A wrapper for the update_post_meta function in wordpress core, as it does not allow for detection of failed updates
		$prev = get_post_meta($id,$key, self::metaSingularity($key));

		if ($prev === $val) {
			// If the value is already set to what it needs to be, do nothing and return true
			return true;
		}

		if (update_post_meta($id,$key,$val)) {
			return true;
		}
		// If it wasn't pre-set to the correct value and still returns false-y, we probably failed.
		return false;
	}

	private static function metaSingularity($key = false)
	{
		static $singularity = false;
		if ($singularity === false) {
			$singularity = apply_filters('msp_supply_meta_singularity',array());
		}
		if ($key) {
			if (array_key_exists($key, $singularity)) {
				return (bool) $singularity[$key];	// If a key is searched and it was supplied, return it's value directly
			}
			return true; // Return a default of true if a key is searched but not supplied
		}
		return $singularity;	// If all else fails, return the whole array.
	}
}

Plugin::init();

/*
Other thoughts
-End result: if you are on the edit page for a post in the valid source post types list for your site
	-create a metabox containing 
		-a checkbox for each target site which is both
			-in the valid targets list
			-contains the same post type as this post in it's valid target array
		-The checkbox links to a 'replicate' meta value
		-On update of the post, check for state of the 'replicate' meta values:
			-on (and there is no value in the 'copy' meta)
				-Create a post on that target site in the same post type, copying over any required taxonomies or attachments
				-Also give that post some metavalue pointing back to this original post
				-Also give this original post a metavalue 'copy' pointing to the copy
			-off (and there is a value in the 'copy' meta)
				-Delete the post on the target site referred to by the 'copy' metavalue
			-on (and there is a value in the 'copy' meta)
				-Update the post on the target site referred to by the 'copy' metavalue

				[Better logic]
					does the copy metavalue exist?
						###if so, delete the post it points at
							[Actually scratch that, this is more likely to interfere with other things like PostViewCounter and will make attachments a hassle. We should determine if an update is possible and use that.]
					is the replicate metavalue true?
						if so, create a post at the target site and fill the copy metavalue in

				[Better Logic again]
					for each site id in either the replicate or the copy metavalue
						if that site id is in both
							state is on-on, update the copy post
						if it is in replicate but not copy
							state is off-on, create the copy post
						if it is in copy but not replicate
							state is on-off, delete the copy post

-On the copied posts:
	-When they are viewed, set a canonical link to the source site's copy of the post
	-When they are edited, 301 redir to the post edit page on the source site

Other Notes:

the replicate key is an auto-indexed array of site id's, but the copy key is an array of post id's indexed by site_id's. 
So remember that if you need the copies array in a format similar to the replicate array you need to do $copies = array_keys($copies);
*/
?>