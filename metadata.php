<?php
/*
	Filters used in this file:
	msp_base_metabox
		Allows to alter the metabox definition array. Called once per page load
	msp_base_metabox_field
		Allows to alter the metabox field definition array. Called once per page load
	msp_metabox_{$type}
		Allows to alter the metabox definition array for a specific type. Called once per valid source type
	msp_metabox_field_{$type}
		Allows to alter the metabox field definition array for a specific type. Called once per valid source type
	msp_metabox_label
		Allows to alter the label for the replication checkboxs. replaces the following identifiers with details from the target site for that checkbox: %id%, %name%, %path%,and %url%.
	msp_metabox_{$type}_label
		Allows to alter the label for the replication checkboxs for a specific type. replaces the following identifiers with details from the target site for that checkbox: %id%, %name%, %path%,and %url%.
	msp_metabox_{$type}_{$target}_label
		Allows to alter the label for the replication checkboxs for a specific type and target. replaces the following identifiers with details from the target site for that checkbox: %id%, %name%, %path%,and %url%.
*/
namespace MultisitePosts;
use RW_Meta_Box, WP_Post, RWMB_Taxonomy_Field;

require_once(ABSPATH . 'wp-admin/includes/screen.php');
abstract class Metadata
{
	public static $KEYS = array(
		'replicate'	=> 'msp_post_replication',
		'copies'	=> 'msp_post_replicated',
		'source'	=> 'msp_post_source',
		'sourceSite'	=> 'msp_post_source_site',
		'wp_attach' => '_wp_attachment_metadata',
		'overrideMetas' => 'msp_override_',
		'addTerms' => 'msp_additional_',
	);
	public static $DISCARDKEYS = array(
		'_edit_lock',
		'_edit_last',
		'_wp_attached_file',
	);
	public static $SUPPORTS = array(
		'title',
		'editor',
		'author',
		'thumbnail',
		'excerpt',
		'trackbacks',
		'custom-fields',
		'comments',
		'revisions',
		'page-attributes',
		'post-formats',
	);
	public static function init()
	{
		$cls = get_class();
		add_action( 'admin_init', array($cls , 'registerSource') , PHP_INT_MAX );
		add_action( 'current_screen', array($cls , 'registerTarget') , PHP_INT_MAX );
		self::bind();
	}

	public static function bind($unbind = false)
	{
		$cb = array(get_class(), 'save');
		if ($unbind) {
			remove_action( 'save_post', $cb, PHP_INT_MAX, 3);
		} else {
			add_action( 'save_post', $cb, PHP_INT_MAX, 3);
		}
	}

	public static function registerSource()
	{
		$settings = Plugin::$SETTINGS;

		// Define the basic settings for RWMB, and allow user filters to modify them
		$base_metabox = apply_filters('msp_base_metabox',array(
			'id' => 'msp-source-post-meta',
			'title' => 'Multisite Post Options',
			'pages' => array(),
			'context' => 'normal',
			'priority' => 'low',
			'fields'	=> array(),
		));
		$base_metabox_field = apply_filters('msp_base_metabox_field',array(
			'name' => 'Replicate this post to:',
			'desc' => 'Choose sites to replicate the post to.',
			'id' => self::$KEYS['replicate'],
			'type' => 'checkbox_list',
			'options' => array()
		));

		// If RWMB doesn't exist, or the user filters have falsified the RWMB data, back out now.
		if (!class_exists('RW_Meta_Box') || !$base_metabox || !$base_metabox_field) {
			return false;
		}

		// Loop through the valid source types...
		foreach ($settings['calculated']['byTarget'] as $type => $targets) {
			// Allow for more specific user filters on the RWMB data, by post type.
			$metabox = apply_filters("msp_metabox_{$type}" , $base_metabox);
			$metabox_field = apply_filters("msp_metabox_field_{$type}" , $base_metabox_field);
			
			// Back out if the user filters for this type falsified the RWMB data
			if (!$metabox || !$metabox_field) {
				continue;
			}

			// Ensure an options array still exists to work with
			if (!array_key_exists('options', $metabox_field)) {
				$metabox_field['options'] = array();
			}

			// Loop through the valid target sites for this post type...
			foreach ($targets as $target) {
				// Get the details about that site to work with
				$targetDetails = get_blog_details($target,true);
				
				// Allow user filters on the label format for the target site checkbox. If the label returns falsey from here, skip adding it.
				$metabox_field_label = apply_filters('msp_metabox_{$type}_{$target}_label', apply_filters('msp_metabox_{$type}_label', apply_filters('msp_metabox_label','%name%')));
				if (!$metabox_field_label) {
					continue;
				}
				// Find occurences of %id%, %name%, %path%,and %url%. Replace with info from the target site.
				$metabox_field_label = str_replace(array('%id%','%name%','%path%','%url%'), array($targetDetails->blog_id,$targetDetails->blogname,$targetDetails->path,$targetDetails->siteurl), $metabox_field_label);
				$metabox_field['options'][$target] = $metabox_field_label;
			}
			// If no options were added, move along without registering an empty metabox
			if (sizeof($metabox_field['options']) < 1) {
				continue;
			}

			// Ensure a fields array still exists to work with
			if (!array_key_exists('fields', $metabox)) {
				$metabox['fields'] = array();
			}
			$metabox['fields'][] = $metabox_field;

			$metabox['pages'] = array($type);

			// Register the metabox
			new RW_Meta_Box($metabox);
		}
	}

	public static function registerTarget()
	{
		if (is_admin()) {
			$postid = $_REQUEST['post'];
			$screen = get_current_screen();
			$cls = get_class();
			if ($screen->base == 'post' && $postid && get_post_meta($postid,Metadata::$KEYS['source'],true)) {
				add_action('edit_form_top', array($cls,'overrideEditPage'), 10);
				RWMB_Taxonomy_Field::admin_enqueue_scripts();
			} elseif ($screen->base == 'edit') {
				add_filter('post_row_actions',array($cls,'overrideEditListActions') , 10, 2);
			}
		}
	}

	public static function save($postID, $post = null)
	{
		if ((defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE) || (defined( 'DOING_AJAX' ) && DOING_AJAX)) {
			return;
		}

		if (!$post instanceof WP_Post) {
			$post = get_post($postID);
			if ($post === null) {
				Plugin::error("Parameter passed for \$post was not a valid WP_Post object, and could not retrieve one based on \$postID. Post ID: {$postID};");
				return;
			}
		}

		if(in_array($post->post_status, array('auto-draft', 'inherit'))) {
			return;
		}

		$meta = get_post_meta($postID);
		$replicate = $meta[self::$KEYS['replicate']] ? $meta[self::$KEYS['replicate']] : array();
		$copies = $meta[self::$KEYS['copies']][0] ? unserialize($meta[self::$KEYS['copies']][0]) : array();

		$copySites = array_keys($copies);

		$sites = array_unique(array_merge($replicate,$copySites));

		foreach ($sites as $site) {
			if ($site == Plugin::$CURRENT_SITE) {
				// abandon hope, all ye who enter here. If this happens, something has gone seriously wrong
				Plugin::error("Self replication attempt detected (WARNING: this can cause blindness). Site: {$site}; Post ID: {$postID};");
				continue;
			}

			$state = array(
				in_array($site, $copySites),
				in_array($site, $replicate),
			);
			
			if ($state[0]) {
				if ($state[1]) {
					// On-On, update the post
					$rep = Plugin::updateReplica($post,$site,$copies[$site]);
					// If it was successful, update the copies array
					if ($rep) {
						$copies[$site] = $rep;
					}
				} else {
					// On-Off, Delete the post
					$rep = Plugin::deleteReplica($post,$site,$copies[$site]);
					// Remove the post from the copies array whether or not it was successful
					unset($copies[$site]);
				}
			} else {
				if ($state[1]) {
					
					// Off-On, Create the post
					$rep = Plugin::createReplica($post,$site);
					// If it was successful, add to the copies array
					if ($rep) {
						$copies[$site] = $rep;
					}
				} else {
					// Off-Off. You shouldn't be here!
					Plugin::error("Attempting to replicate in an invalid state. Site: {$site}; Post ID: {$postID};");
				}
			}
		}
		update_post_meta($postID, self::$KEYS['copies'], $copies);
	}

	public static function overrideEditListActions($actions, $post)
	{
		if ($sourcePost = get_post_meta($post->ID,Metadata::$KEYS['source'],true)) {
			unset($actions['inline hide-if-no-js']);
			
			$sourceSite = get_post_meta($post->ID,Metadata::$KEYS['sourceSite'],true);
			$sourceSiteDetails = get_blog_details($sourceSite,true);
			$actions['msp'] = self::anchor('Edit Source', $sourceSiteDetails->siteurl . '/wp-admin/post.php?post=' . $sourcePost . '&action=edit');
		}
		return $actions;
	}

	public static function overrideEditPage()
	{
		global $wp_meta_boxes, $post;
		self::remove_post_type_support($post->post_type, self::$SUPPORTS);
		// remove all meta boxes
		$wp_meta_boxes = array($post->post_type => array(
			'advanced' => array(),
			'side' => array(),
			'normal' => array(),
		));

		$sourcePost = get_post_meta($post->ID,Metadata::$KEYS['source'],true);
		$sourceSite = get_post_meta($post->ID,Metadata::$KEYS['sourceSite'],true);
		$sourceSiteDetails = get_blog_details($sourceSite,true);
		
		$notice = 'This is a replicated post from the site: ';
		$notice .= self::anchor($sourceSiteDetails->blogname, $sourceSiteDetails->siteurl);
		$notice .= '. Rather than editing this post, you should ';
		$notice .= self::anchor('edit the source post', $sourceSiteDetails->siteurl . '/wp-admin/post.php?post=' . $sourcePost . '&action=edit') . '.';

		$taxonomies = apply_filters('msp_allow_additional_terms', array(), $post);
		$metas = apply_filters('msp_allow_override_metas', array(), $post);

		if (sizeof($taxonomies) < 1 && sizeof($metas) < 1) {
			echo self::notice($notice);
			return;
		}
		$notice .= ' You can use this screen to ';
		echo self::tag('form','',array(),true);
		if (sizeof($taxonomies) > 0) {
			$notice .= 'set up taxonomies (tags, categories, etc) that will apply to this post, but NOT the source post';
		}
		if (sizeof($taxonomies) > 0 && sizeof($metas) > 0) {
			$notice .= ' or';
		}

		if (sizeof($metas) > 0) {
			$notice .= ' set up metadata that will OVERRIDE the metadata in the source post';
		}
		$notice .= '.';
		echo self::notice($notice);
		

		echo self::tag('form','',array('action'=>'post.php','method'=>'get','autocomplete'=>'off'));
		echo self::tag('input','',array('type'=>'hidden','name'=>'post','value'=>$post->ID,));
		echo self::tag('input','',array('type'=>'hidden','name'=>'action','value'=>'edit',));
		echo self::tag('input','',array('type'=>'hidden','name'=>'updated','value'=>'yes',));

		$fieldArgs = array(
			'type' => 'taxonomy',
			'placeholder' => 'Select Terms...',
			'multiple' => true,
			'options' => array('placeholder' => 'Select Terms...','type'=>'select_advanced', 'args'=>array('hide_empty'=>false)),
			'js_options' => array('allowClear' => true, 'width' => '60%', 'placeholder' => 'Select Terms...'),
		);
		$html = '';
		foreach ($taxonomies as $taxonomy) {
			$taxonomy = get_taxonomy($taxonomy);
			$id = self::$KEYS['addTerms'] . $taxonomy->name;
			
			$prev = $value = get_post_meta($post->ID,$id, true);
			//First check for updated values
			if (isset($_GET['updated'])) {
				if (isset($_GET[$id])) {
					$value = array_filter(array_map('intval',(array) $_GET[$id]), 'is_numeric');
					if ($prev !== $value) {
						update_post_meta($post->ID,$id,$value);
					}
					if (!$prev) {
						$prev = array();
					}
					$new = array_diff($value,$prev);
					$removed = array_diff($prev, $value);
					wp_remove_object_terms( $post->ID, $removed, $taxonomy->name);
					wp_add_object_terms( $post->ID, $new, $taxonomy->name);
				} elseif ($prev) {
					wp_remove_object_terms( $post->ID, $prev, $taxonomy->name);
					delete_post_meta($post->ID,$id);
					$value = array();
				}
			}

			// Then build the form
			if (!$value) {
				$value = array();
			}

			$fieldArgs['name'] = $taxonomy->labels->name;
			$fieldArgs['id'] = $id;
			$fieldArgs['field_name'] = $id . '[]';
			$fieldArgs['options']['taxonomy'] = $taxonomy->name;

			$fieldArgs = apply_filters('msp_additional_terms_field_args', $fieldArgs, $post, $taxonomy);
			$html .= RWMB_Taxonomy_Field::begin_html($value, $fieldArgs);
			$html .= RWMB_Taxonomy_Field::html($value, $fieldArgs);
			$html .= RWMB_Taxonomy_Field::end_html($value, $fieldArgs);
		}

		echo self::postbox($html,'Taxonomies');
		echo '<br/>';

		$html = '';
		foreach ($metas as $meta => $single) {
			$id = self::$KEYS['overrideMetas'] . $meta;
			
			$prev = $value = get_post_meta($post->ID,$id,$single);
			//First check for updated values
			if (isset($_GET['updated'])) {
				if (isset($_GET[$id])) {
					$value = apply_filters('msp_override_metas_value',$_GET[$id], $post, $meta, $single);
					update_post_meta($post->ID,$id,$value);
					update_post_meta($post->ID,$meta,$value);
				}
			}

			// Then build the form
			$html .= apply_filters('msp_override_metas_field_html', '', $value, $post, $meta, $id, $single);
		}
		if (strlen($html) > 0) {
			echo self::postbox($html,'Meta');
		}
		echo '<br/>';
		
		if (isset($_GET['updated'])) {
			do_action('msp_replica_options_updated', $post);
		}

		echo self::wrap('Update', 'button');
	}

	// Utility wrapper to remove multiple supports at once
	private static function remove_post_type_support($type,$features = array())
	{
		foreach ($features as $feature) {
			remove_post_type_support($type,$feature);
		}
	}

	private static function postbox($contents,$title) {
		return self::wrap(self::wrap(self::wrap($title,'span'),'h3','hndle') . $contents, 'div', 'postbox');
	}

	private static function anchor($text, $href, $class = '')
	{
		return self::wrap($text,'a',$class, array('href'=>$href));
	}

	private static function notice($text, $class = 'error')
	{
		return self::wrap($text,'div',$class);
	}

	private static function wrap($text, $tag = 'div', $class = '', $attribs = array())
	{
		return self::tag($tag,$class,$attribs) . $text . self::tag($tag,$class,$attribs,true);
	}

	private static function tag($name = 'div', $class='', $attribs = array(), $close = false)
	{
		$r = '<';
		if ($close) {
			$r .= '/';
		}
		$r .= $name;
		if ($close) {
			return $r . '>';
		} 
		if (strlen($class) > 0) {
			$r .= ' class="' . $class . '"';
		}
		if (sizeof($attribs) > 0) {
			foreach ($attribs as $attrib => $value) {
				$r .= ' ' . $attrib . '="' . $value . '"';
			}
		}
		return $r . '>';
	}
}